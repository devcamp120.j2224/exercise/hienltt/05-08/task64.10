package com.devcamp.voucher_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.voucher_api.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long>{
    
}
